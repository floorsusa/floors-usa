Floors USA is a family-run, full service flooring retailer offering a variety of products, including hardwood, tile and laminate flooring, as well as carpet and area rugs. They offer competitive pricing and a 100% satisfaction guarantee, so you can feel confident and comfortable with your purchase.

Address: 555 S Henderson Road, King of Prussia, PA 19406, USA

Phone: 610-757-4000
